﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum Screen{
    MainMenu, Password, Win
};


public class Hacker : MonoBehaviour {

    const string MENU_HINT = "You may type menu at any time";
    string[] passwordsLevelOne = {
        "shelf", "libraian", "reading", "learning"
    };
    string[] passwordsLevelTwo = {
        "handcuffs", "officer", "lawful", "prison"
    };
    string[] passwordsLevelThree = {
        "astronaut", "exploration", "universe", "andromeda"
    };

    int level;
    Screen currentScreen;
    string password;

	// Use this for initialization
	void Start () {
        ShowMainMenu();
	}

    void ShowMainMenu() {
        currentScreen = Screen.MainMenu;
        Terminal.ClearScreen();
        Terminal.WriteLine("What would you like to hack into?\n");
        Terminal.WriteLine("Press 1 for the local library");
        Terminal.WriteLine("Press 2 for the police station");
        Terminal.WriteLine("Press 3 for NASA\n");
        Terminal.WriteLine("Enter your selection: ");
    }

    void OnUserInput(string input) {

        if (input.Equals("menu", StringComparison.InvariantCultureIgnoreCase)) { // Return to main menu
            ShowMainMenu();
        } else if (currentScreen == Screen.MainMenu) {
            ValidateLevelInput(input);
        } else if (currentScreen == Screen.Password) {
            CheckPassword(input);
        } else if (input.Equals("quit", StringComparison.InvariantCultureIgnoreCase) || input.Equals("exit", StringComparison.InvariantCultureIgnoreCase) || input.Equals("close", StringComparison.InvariantCultureIgnoreCase)) {
            Application.Quit();
        } else {
            ShowMainMenu();
        }
    }

    private void CheckPassword(string input)
    {
        if (input == password) { 
            RunWinScreen();
        }
        else {
            AskForPassword();
        }
    }

    void RunWinScreen() {
        currentScreen = Screen.Win;
        Terminal.ClearScreen();
        ShowLevelReward();
    }

    void ValidateLevelInput(string input)
    {
        bool isValidLevelNumber = (input == "1" || input == "2" || input == "3");
        if (isValidLevelNumber) {
            level = int.Parse(input);
            AskForPassword();
        }
        else {
            Terminal.WriteLine("\n" + MENU_HINT + "\n" + input + " is not a valid level.\nPlease choose a valid level.");
        }
    }

    void AskForPassword() {
        currentScreen = Screen.Password;
        Terminal.ClearScreen();
        SetRandomPassword();
        Terminal.WriteLine(MENU_HINT + "\n" + "Enter your password, hint: " + password.Anagram());
    }

    void SetRandomPassword() {
        switch (level) {
            case 1:
                password = passwordsLevelOne[UnityEngine.Random.Range(0, passwordsLevelOne.Length)];
                break;
            case 2:
                password = passwordsLevelTwo[UnityEngine.Random.Range(0, passwordsLevelTwo.Length)];
                break;
            case 3:
                password = passwordsLevelThree[UnityEngine.Random.Range(0, passwordsLevelThree.Length)];
                break;
            default:
                Debug.LogError("Invalid level number");
                break;
        }
    }

    void ShowLevelReward() {
        switch (level) {
            case 1:
                Terminal.WriteLine("Have a book...");
                Terminal.WriteLine(@"
    _________
   /       //
  /       //
 /_______//
(_______(/   
                ");
                Terminal.WriteLine("Hit enter to return to the main menu.");
                break;
            case 2:
                Terminal.WriteLine("Have a gun...");
                Terminal.WriteLine(@"
    ___)____________
  \\   __[]__________|
  / /\ /  ))
 /____|
 \___/
                ");
                Terminal.WriteLine("Hit enter to return to the main menu.");
                break;
            case 3:
                Terminal.WriteLine("Have a telescope...");
                Terminal.WriteLine(@"
   _____________
  ()____________)
       //\\
      //  \\
     //    \\
                ");
                Terminal.WriteLine("Hit enter to return to the main menu.");
                break;
        }
    }
}
